# blaseball\_rp2040

## Goal
Display a Blaseball game on as many devices as possible through an RP2040, with the minimum amount of external hardware.

## Hardware
Currently, an Adafruit Feather RP2040, Segger J-Link, and [Not HDMI Featherwing](https://github.com/leo60228/not-hdmi-featherwing) are required. It should be easy to adapt to other hardware supported by PicoDVI.

Planned additions will require the following hardware:
* [LAN8720 module](https://www.amazon.com/KOOBOOK-Electronics-High-Performance-Transceiver-Development/dp/B07S8MRH92)
* [GBA link cable breakout](https://www.tindie.com/products/vaguilar/gameboy-coloradvancesp-link-cable-breakout-board)
* Game Boy Advance
* Nintendo Switch

## Running
Same as any other pico-sdk project. A Nix flake development environment is provided, as well as `flash`, `flash_rtt`, and `flash_gdb` targets.

## Components
### Fetching games
* Status: 5%
  * Temporarily implemented via Python code generation
* Components
  * Network stack
    * Status: 0%
    * Reference docs/code
      * https://github.com/strags/pico-rmiieth
  * TLS
    * Status: 0%
    * Reference docs/code
      * https://tls.mbed.org/kb/how-to/how-do-i-port-mbed-tls-to-a-new-environment-OS
      * http://git.savannah.gnu.org/cgit/lwip.git/tree/src/apps/altcp_tls
      * https://www.nongnu.org/lwip/2_1_x/group__altcp__api.html
    * Notes
      * Probably CPU-intensive
      * Don't have enough storage for root CAs, maybe pin certificates?
  * HTTP
    * Status: 0%
    * Reference docs/code
      * http://git.savannah.gnu.org/cgit/lwip.git/tree/src/apps/http/http_client.c
    * Notes
      * Probably just hardcode the data to send to the server? Doesn't need to be flexible

### Display on DVI
* Status: 80%
  * Working but hacky
* Components
  * Terminal
    * Status: 80%
      * No line wrapping, first line at bottom of screen
    * Reference docs/code
      * https://github.com/Wren6991/PicoDVI/blob/master/software/apps/terminal/main.c
      * https://github.com/Wren6991/micropython/blob/dvi-vt100/ports/rp2/dvi_stdout.c

### Display on Switch
* Status: 50%
  * Not finished but lots of progress
* Components
  * USB stack
    * Status: 70%
      * Works but very buggy
    * Reference docs/code
      * https://github.com/hathach/tinyusb/blob/master/src/portable/raspberrypi/rp2040/hcd_rp2040.c
      * https://github.com/hathach/tinyusb/blob/master/src/portable/raspberrypi/rp2040/rp2040_usb.c
      * https://github.com/hathach/tinyusb/blob/master/src/host/usbh.c
      * https://github.com/raspberrypi/pico-examples/blob/master/usb/device/dev_lowlevel/dev_lowlevel.c
  * Fusée Gelée
    * Status: 95%
      * Mostly done but not well-tested because of USB stack bugs
    * Reference docs/code
      * https://github.com/Qyriad/fusee-launcher/blob/master/report/fusee_gelee.md
      * https://github.com/Qyriad/fusee-launcher/blob/master/fusee-launcher.py
      * https://github.com/DavidBuchanan314/fusee-nano/tree/master/src
  * Payload
    * Status: 0%
    * Reference docs/code
      * https://github.com/shchmue/Lockpick_RCM
      * https://github.com/rajkosto/memloader/blob/master/src/rcm_usb.c
  * Communicating with Switch
    * Status: 0%
    * Notes
      * Probably not that hard, very similar to Fusée

### Display on GBA
* Status: 0%
* Components
  * Multiboot
    * Status: 0%
    * Reference docs/code
      * https://mgba-emu.github.io/gbatek/#sionormalmode (note: page is very large)
      * http://spritesmods.com/gbamidi/gbamidi-v1.0.tgz
      * https://github.com/akkera102/gba_03_multiboot/blob/master/src/multiboot.c
  * GBA code
    * Status: 0%
    * Reference docs/code
      * https://github.com/rust-console/gba
  * GBA communications
    * Status: 0%
    * Notes
      * Shares most code with multiboot
