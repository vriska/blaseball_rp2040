#!/usr/bin/env bash
set -e
set -o pipefail

case "$1" in
  flash)
    program=1
    program_exit=1
    ;;

  flash_rtt)
    program=1
    rtt=1
    ;;

  flash_gdb)
    program=1
    gdb=1
    ;;

  *)
    echo "unknown mode $1" >&2
    exit 1
esac

openocd_args=()

load_file() {
  openocd_args+=(-f "$1")
}

run_command() {
  openocd_args+=(-c "$1")
}

connect_once_ready() {
  tee "$3" | while read line; do
    if [[ "$line" == *"for $1 connections"* ]]; then
      (eval "$2") <&3
      kill $$
      return
    fi
  done
}

kill_subprocesses() {
  kill $(jobs -p) 2>/dev/null || true
}

load_file interface/jlink.cfg
run_command "adapter speed 4000"
run_command "transport select swd"
load_file target/rp2040.cfg

if ! [ -z "$program" ]; then
  run_command "program blaseball_rp2040.hex verify reset ${program_exit:+exit}"
fi

exec 3<&0

if ! [ -z "$rtt" ]; then
  run_command 'rtt setup 0x20000000 0x42000 "SEGGER RTT"'
  run_command "rtt start"
  run_command "rtt server start 9090 0"

  trap 'kill_subprocesses' EXIT

  openocd "${openocd_args[@]}" 2> >(connect_once_ready rtt 'nc localhost 9090' /dev/stderr)
elif ! [ -z "$gdb" ]; then
  gdb_cmd='arm-none-eabi-gdb -ex "target extended-remote localhost:3333" blaseball_rp2040.elf'

  trap 'kill_subprocesses' EXIT

  openocd "${openocd_args[@]}" 2> >(connect_once_ready gdb "set -m; $gdb_cmd" openocd.log)
else
  openocd "${openocd_args[@]}"
fi
