#!/usr/bin/env python3
from blaseball_mike.models import Team, Game
from blaseball_mike.chronicler import get_game_updates

def to_c_string(s):
    escaped = s.replace('\\', '\\\\').replace('\n', '\\n').replace('"', '\\"')
    return f'"{escaped}"'

crabs = Team.load_by_name('crabs')
game = next(iter(Game.load_by_season(13, day=116, team_id=crabs.id).values()))

print('#pragma once\n')

print('const char* game_updates[] = {');

count = 0
for update in get_game_updates(game_ids=[game.id], order='asc', lazy=True):
    string = to_c_string(update['data']['lastUpdate'])
    print(f'    {string},')
    count += 1

print('};\n')

print(f'const int game_updates_len = {count};')
