#pragma once

#include <stdint.h>
#include <usb/usb_common.h>

void usb_host_init();
void usb_host_send(uint8_t dev_addr, uint8_t endpoint, const uint8_t* buf, int len, void (*callback)());
void usb_host_send_setup_packet_only(uint8_t dev_addr, uint8_t endpoint, struct usb_setup_packet packet, void (*callback)());
void usb_host_recv_one(uint8_t dev_addr, uint8_t endpoint, uint8_t* buffer, int len, void (*callback)());
