#include <pico/stdlib.h>
#include <hardware/resets.h>
#include <hardware/irq.h>
#include <hardware/structs/usb.h>
#include <usb/usb_common.h>
#include <string.h>
#include <stdio.h>

#include "fusee.h"

#define SIE_CTRL_BASE (USB_SIE_CTRL_SOF_EN_BITS | USB_SIE_CTRL_KEEP_ALIVE_EN_BITS | USB_SIE_CTRL_PULLDOWN_EN_BITS | USB_SIE_CTRL_EP0_INT_1BUF_BITS)

typedef enum {
    STATE_START,
    STATE_WAITING_FOR_DEVICE_DESC,
    STATE_ACKED_DEVICE_DESC,
    STATE_WAITING_FOR_SET_ADDR,
    STATE_ACKED_SET_ADDR,
    STATE_WAITING_FOR_SET_CONFIG,
    STATE_ACKED_SET_CONFIG,
    STATE_IDLE,
    STATE_SENDING_DATA,
    STATE_RECEIVING_DATA,
    STATE_SENDING_SETUP_PACKET,
    STATE_DONE,
} usb_host_state;

usb_host_state current_state = STATE_START;

uint8_t send_dev_addr;
uint8_t send_endpoint;
const uint8_t* send_source = NULL;
int send_len = 0;
int send_pos = 0;
int send_pid = 0;

uint8_t* recv_target = NULL;
int recv_len = 0;

void (*transfer_callback)();

static void usb_host_xfer_one(uint8_t dev_addr, uint8_t endpoint, int pos, int len, int sie_ctrl, int epx_ctrl, int pid, bool last) {
    usb_hw->dev_addr_ctrl = dev_addr | (endpoint << USB_ADDR_ENDP_ENDPOINT_LSB);
    usbh_dpram->epx_ctrl = EP_CTRL_ENABLE_BITS | (pos + 0x180) | epx_ctrl;
    usbh_dpram->epx_buf_ctrl = (last ? USB_BUF_CTRL_LAST : 0) | (pid ? USB_BUF_CTRL_DATA1_PID : 0) | len;
    usbh_dpram->epx_buf_ctrl |= USB_BUF_CTRL_AVAIL | ((sie_ctrl & USB_SIE_CTRL_SEND_DATA_BITS) ? USB_BUF_CTRL_FULL : 0);
    usb_hw->sie_ctrl = SIE_CTRL_BASE | sie_ctrl;
}

void usb_host_recv_one(uint8_t dev_addr, uint8_t endpoint, uint8_t* buffer, int len, void (*callback)()) {
    assert(current_state == STATE_IDLE);

    recv_target = buffer;
    recv_len = len;

    transfer_callback = callback;

    printf("receiving %d byte chunk\n", len);

    current_state = STATE_RECEIVING_DATA;

    usb_host_xfer_one(dev_addr, endpoint, 0, len, USB_SIE_CTRL_RECEIVE_DATA_BITS, USB_TRANSFER_TYPE_BULK << EP_CTRL_BUFFER_TYPE_LSB, 0, true);
}

static void usb_host_recv_done() {
    puts("received chunk");

    memcpy(recv_target, &usbh_dpram->epx_data[0], recv_len);

    recv_target = NULL;
    recv_len = 0;

    current_state = STATE_IDLE;

    void (*callback)() = transfer_callback;
    if (callback) {
        transfer_callback = NULL;
        callback();
    }
}

static void usb_host_send_done() {
    puts("send done");

    send_dev_addr = 0;
    send_endpoint = 0;
    send_source = NULL;
    send_len = 0;
    send_pos = 0;
    send_pid = 0;

    current_state = STATE_IDLE;

    void (*callback)() = transfer_callback;
    if (callback) {
        transfer_callback = NULL;
        callback();
    }
}

static void usb_host_send_chunk(bool first) {
    int chunk_size = send_len - send_pos;
    bool last = false;

    printf("%d bytes remaining\n", chunk_size);

    if (chunk_size > 64) {
        chunk_size = 64;
    } else if (chunk_size == 0) {
        return;
    } else {
        last = true;
    }

    printf("sending %d byte chunk (%s)\n", chunk_size, last ? "last" : "not last");

    memcpy(&usbh_dpram->epx_data[0x100], send_source + send_pos, chunk_size);
    send_pos += chunk_size;

    usb_host_xfer_one(send_dev_addr, send_endpoint, 0x100, chunk_size, USB_SIE_CTRL_SEND_DATA_BITS | (first ? USB_SIE_CTRL_START_TRANS_BITS : 0), (USB_TRANSFER_TYPE_BULK << EP_CTRL_BUFFER_TYPE_LSB) | (last ? 0 : EP_CTRL_INTERRUPT_PER_BUFFER), send_pid, last);
    send_pid ^= 1;
}

void usb_host_send(uint8_t dev_addr, uint8_t endpoint, const uint8_t* buf, int len, void (*callback)()) {
    assert(current_state == STATE_IDLE);

    send_dev_addr = dev_addr;
    send_endpoint = endpoint;
    send_source = buf;
    send_len = len;
    send_pos = 0;

    transfer_callback = callback;

    current_state = STATE_SENDING_DATA;

    usb_host_send_chunk(true);
}

void usb_host_send_setup_packet_only(uint8_t dev_addr, uint8_t endpoint, struct usb_setup_packet packet, void (*callback)()) {
    assert(current_state == STATE_IDLE);

    current_state = STATE_SENDING_SETUP_PACKET;

    *((struct usb_setup_packet*) &usbh_dpram->setup_packet) = packet;
    usb_hw->dev_addr_ctrl = dev_addr | (endpoint << USB_ADDR_ENDP_ENDPOINT_LSB);
    usbh_dpram->epx_ctrl = EP_CTRL_ENABLE_BITS | 0x180;
    usb_hw->sie_ctrl = SIE_CTRL_BASE | USB_SIE_CTRL_SEND_SETUP_BITS | USB_SIE_CTRL_START_TRANS_BITS;
}

static void usb_host_send_setup_packet(uint8_t dev_addr, uint8_t endpoint, struct usb_setup_packet packet, int pos, int send_recv) {
    *((struct usb_setup_packet*) &usbh_dpram->setup_packet) = packet;
    usb_host_xfer_one(dev_addr, endpoint, pos, packet.wLength, send_recv | USB_SIE_CTRL_SEND_SETUP_BITS | USB_SIE_CTRL_START_TRANS_BITS, 0, 1, true);
}

static int64_t usb_host_reset_delay_done(alarm_id_t alarm, void* data) {
    puts("reset delay done");

    // make sure it's still there
    if (!(usb_hw->sie_status & USB_SIE_STATUS_SPEED_BITS)) return 0;

    // make device descriptor request (TODO: does EP0 need to be opened?)
    struct usb_setup_packet get_descriptor = {
        .bmRequestType = USB_REQ_TYPE_RECIPIENT_DEVICE | USB_REQ_TYPE_STANDARD | USB_DIR_IN,
        .bRequest = USB_REQUEST_GET_DESCRIPTOR,
        .wValue = USB_DT_DEVICE << 8,
        .wIndex = 0,
        .wLength = sizeof(struct usb_device_descriptor)
    };

    puts("requesting device descriptor");

    // send request
    current_state = STATE_WAITING_FOR_DEVICE_DESC;
    usb_host_send_setup_packet(0, 0, get_descriptor, 0, USB_SIE_CTRL_RECEIVE_DATA_BITS);

    return 0;
}

static void usb_host_handle_connection() {
    puts("device connected");

    // set state
    current_state = STATE_START;

    // reset delay (TODO: is this the best way to handle this?)
    add_alarm_in_ms(500, usb_host_reset_delay_done, NULL, true);
}

static void usb_host_handle_disconnection() {
    puts("device disconnected");

    // set state
    current_state = STATE_START;
}

static void usb_host_received_device_descriptor_ack() {
    puts("ack");
    current_state = STATE_ACKED_DEVICE_DESC;
    usb_host_xfer_one(0, 0, 0, 0, USB_SIE_CTRL_SEND_DATA_BITS | USB_SIE_CTRL_START_TRANS_BITS, EP_CTRL_INTERRUPT_PER_BUFFER, 0, true);
}

static void usb_host_received_device_descriptor() {
    struct usb_device_descriptor* desc = (struct usb_device_descriptor*) &usbh_dpram->epx_data[0];

    if (desc->bDescriptorType != USB_DT_DEVICE) {
        printf("bad descriptor type %d!\n", desc->bDescriptorType);
        current_state = STATE_DONE;
        return;
    }

    printf("got device descriptor for %04x:%04x\n", desc->idVendor, desc->idProduct);

    // make set address request
    struct usb_setup_packet set_address = {
        .bmRequestType = USB_REQ_TYPE_RECIPIENT_DEVICE | USB_REQ_TYPE_STANDARD | USB_DIR_OUT,
        .bRequest = USB_REQUEST_SET_ADDRESS,
        .wValue = 1,
        .wIndex = 0,
        .wLength = 0
    };

    puts("setting address");

    // send packet
    current_state = STATE_WAITING_FOR_SET_ADDR;
    usb_host_send_setup_packet(0, 0, set_address, 0, 0);
}

static void usb_host_addr_set_ack() {
    puts("ack");
    current_state = STATE_ACKED_SET_ADDR;
    usb_host_xfer_one(0, 0, 0, 0, USB_SIE_CTRL_RECEIVE_DATA_BITS | USB_SIE_CTRL_START_TRANS_BITS, 0, 1, true);
}

static void usb_host_addr_set() {
    puts("set address");

    // make set configuration packet
    struct usb_setup_packet set_configuration = {
        .bmRequestType = USB_REQ_TYPE_RECIPIENT_DEVICE | USB_REQ_TYPE_STANDARD | USB_DIR_OUT,
        .bRequest = USB_REQUEST_SET_CONFIGURATION,
        .wValue = 1,
        .wIndex = 0,
        .wLength = 0
    };

    puts("setting configuration");

    // send packet
    current_state = STATE_WAITING_FOR_SET_CONFIG;
    usb_host_send_setup_packet(1, 0, set_configuration, 0, 0);
}

static void usb_host_device_configured_ack() {
    puts("ack");
    current_state = STATE_ACKED_SET_CONFIG;
    usb_host_xfer_one(1, 0, 0, 0, USB_SIE_CTRL_RECEIVE_DATA_BITS | USB_SIE_CTRL_START_TRANS_BITS, 0, 1, true);
}

static void usb_host_device_configured() {
    puts("device configured");

    // update state
    current_state = STATE_IDLE;

    // trigger exploit
    exploit();
}

static void usb_host_irq() {
    uint32_t status = usb_hw->ints;

    printf("irq %08x\n", status);

    if (status & USB_INTS_STALL_BITS) {
        puts("stalled");

        // error
        current_state = STATE_DONE;

        // acknowledge
        hw_clear_alias(usb_hw)->sie_status = USB_SIE_STATUS_STALL_REC_BITS;
    }

    if (status & USB_INTS_ERROR_RX_TIMEOUT_BITS) {
        puts("timeout");

        // error
        current_state = STATE_DONE;

        // acknowledge
        hw_clear_alias(usb_hw)->sie_status = USB_SIE_STATUS_RX_TIMEOUT_BITS;
    }

    if (status & USB_INTS_ERROR_DATA_SEQ_BITS) {
        puts("data seq error");

        // error
        current_state = STATE_DONE;

        // acknowledge
        hw_clear_alias(usb_hw)->sie_status = USB_SIE_STATUS_DATA_SEQ_ERROR_BITS;
    }

    if (status & USB_INTS_HOST_CONN_DIS_BITS) {
        if (usb_hw->sie_status & USB_SIE_STATUS_SPEED_BITS) {
            usb_host_handle_connection();
        } else {
            usb_host_handle_disconnection();
        }

        // acknowledge
        hw_clear_alias(usb_hw)->sie_status = USB_SIE_STATUS_SPEED_BITS;
    }

    bool trans_complete = false;
    if (status & USB_INTS_TRANS_COMPLETE_BITS) {
        trans_complete = true;

        // acknowledge
        hw_clear_alias(usb_hw)->sie_status = USB_SIE_STATUS_TRANS_COMPLETE_BITS;
    }

    bool buff_status = false;
    if (status & USB_INTS_BUFF_STATUS_BITS) {
        buff_status = true;

        // acknowledge
        usb_hw->buf_status = 0;
    }

    if (trans_complete || buff_status) {
        if (current_state == STATE_WAITING_FOR_DEVICE_DESC) {
            usb_host_received_device_descriptor_ack();
        } else if (current_state == STATE_ACKED_DEVICE_DESC) {
            usb_host_received_device_descriptor();
        } else if (current_state == STATE_WAITING_FOR_SET_ADDR) {
            usb_host_addr_set_ack();
        } else if (current_state == STATE_ACKED_SET_ADDR) {
            usb_host_addr_set();
        } else if (current_state == STATE_WAITING_FOR_SET_CONFIG) {
            usb_host_device_configured_ack();
        } else if (current_state == STATE_ACKED_SET_CONFIG) {
            usb_host_device_configured();
        } else if (current_state == STATE_SENDING_SETUP_PACKET) {
            usb_host_send_done();
        } else if (current_state == STATE_RECEIVING_DATA) {
            usb_host_recv_done();
        } else if (trans_complete && current_state == STATE_SENDING_DATA) {
            usb_host_send_done();
        } else if (buff_status && current_state == STATE_SENDING_DATA) {
            usb_host_send_chunk(false);
        }
    }

    if (status & USB_INTS_HOST_RESUME_BITS) {
        puts("resumed");

        // acknowledge
        hw_clear_alias(usb_hw)->sie_status = USB_SIE_STATUS_RESUME_BITS;
    }
}

void usb_host_init() {
    puts("initializing usb");

    // reset
    reset_block(RESETS_RESET_USBCTRL_BITS);
    unreset_block_wait(RESETS_RESET_USBCTRL_BITS);

    // clear dpram
    memset(usbh_dpram, 0, sizeof(*usbh_dpram));

    // mux
    usb_hw->muxing = USB_USB_MUXING_TO_PHY_BITS | USB_USB_MUXING_SOFTCON_BITS;

    // enable interrupt
    irq_set_exclusive_handler(USBCTRL_IRQ, usb_host_irq);
    irq_set_enabled(USBCTRL_IRQ, true);

    // vbus detect
    usb_hw->pwr = USB_USB_PWR_VBUS_DETECT_BITS | USB_USB_PWR_VBUS_DETECT_OVERRIDE_EN_BITS;

    // host mode
    usb_hw->main_ctrl = USB_MAIN_CTRL_CONTROLLER_EN_BITS | USB_MAIN_CTRL_HOST_NDEVICE_BITS;

    // sie_ctrl and interrupts
    usb_hw->sie_ctrl = SIE_CTRL_BASE;
    usb_hw->inte = USB_INTE_BUFF_STATUS_BITS | USB_INTE_HOST_CONN_DIS_BITS | USB_INTE_HOST_RESUME_BITS | USB_INTE_STALL_BITS | USB_INTE_TRANS_COMPLETE_BITS | USB_INTE_ERROR_RX_TIMEOUT_BITS | USB_INTE_ERROR_DATA_SEQ_BITS;
}
